FROM python:2.7
MAINTAINER Ido Shilon "ido@identiq.com"
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["az-server.py"]
