#!flask/bin/python
from flask import Flask, jsonify, request, make_response, Response, json, render_template, redirect, url_for
from flaskext.mysql import MySQL
import uuid
import md5
import logging
import os
import logging
import httplib2
import urllib2
from os import environ
from logging.handlers import RotatingFileHandler
import logging.config
from forms import SignupForm
from flask_bootstrap import Bootstrap
from werkzeug.contrib.cache import SimpleCache
import textwrap
from beautifultable import BeautifulTable
# import loggly.handlers


app = Flask(__name__, static_url_path="")

if environ.get('AZPOC_CONFIG_FILE') is not None:
    app.config.from_envvar('AZPOC_CONFIG_FILE')
    if app.config.from_envvar('AZPOC_CONFIG_FILE') == 'config/single/config.py':
        validators_list_config_file = open('config/single/validatorslist.json')
        matchers_list_config_file = open('config/single/matcherslist.json')
    else:
        validators_list_config_file = open('config/validatorslist.json')
        matchers_list_config_file = open('config/matcherslist.json')
else:
    app.config.from_pyfile(os.path.join(
        ".", "config/single/config.py"), silent=False)
    validators_list_config_file = open('config/single/validatorslist.json')
    matchers_list_config_file = open('config/single/matcherslist.json')


mysql = MySQL()
mysql.init_app(app)

conn = mysql.connect()
# cancel query cache
conn.autocommit(True)
cursor = conn.cursor()

matcher_cache = SimpleCache()
matcher_cache.clear()

aggregator_cache = SimpleCache()
aggregator_cache.clear()

validators_list = json.load(validators_list_config_file)
matchers_list = json.load(matchers_list_config_file)
matchers_count = len(matchers_list['matchers'])


formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
file_handler = logging.FileHandler(app.config["LOG_FILE_NAME"])
file_handler.setFormatter(formatter)

app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)


# logging.config.fileConfig('python.conf')
# logger = logging.getLogger('myLogger')

# logger.info('Test log')


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/az/api/v1.0/requestor/', methods=['POST'])
def request_full_validation():
    request_id = str(uuid.uuid4())

    app.logger.info('generate request id ' + request_id)
    app.logger.info('request_validation ' + str(request.data))

    requaster_fields_data = json.loads(request.data)

    matcher_fields_dict = dict()

    email_validator_fregment = ''
    email_hash = ''
    email_matcher_fregment = ''

    matcher_fields_dict = dict()

    for matcher in matchers_list['matchers']:
        matcher_fields_dict.update(
            {matcher['name']: dict()})

    # for f in requaster_fields_data:
    for requestor_field_name, requestor_field_value in requaster_fields_data.iteritems():
        field_hash_value = md5.new(requestor_field_value).hexdigest()
        # only for email , break the original request to validaors vs matchers
        if requestor_field_name == 'email':
            # starts with 0
            email_hash = field_hash_value
            email_validator_fregment = field_hash_value[0:2]
            field_hash_value_for_fregments = field_hash_value[2:32]

        else:
            field_hash_value_for_fregments = field_hash_value
        app.logger.info('Preparing validator key fregment ' + email_validator_fregment)
        app.logger.info('Preparing data to be sent to mutiple matchers')

        # break each field to list of fregements for multiple matchers `
        for idx, field_hash_fregment in enumerate(textwrap.wrap(field_hash_value_for_fregments, len(field_hash_value_for_fregments) / matchers_count)):
            dict_key = requestor_field_name
            app.logger.debug('field name ' + dict_key)
            app.logger.debug(
                'fregment ' + field_hash_fregment +  ' for matcher ' +matcher_fields_dict.keys()[idx])

            matcher_fields_dict[matcher_fields_dict.keys()[idx]].update(
                {dict_key: field_hash_fregment})


    # call validators first
    for validator in validators_list['validators']:
        validator_call_suceesfull = call_validator(validator['port'], email_validator_fregment,
                                                   request_id, validator['name'], validator['schema'])

    matching_results_dict = {
        'request_email_hash': email_hash,
        'request_id': request_id,
        'matching_results': dict(),
    }

    app.logger.info(
        'in requester  get matcher results and send to matcher only the following fregment ' + email_matcher_fregment)
    # call matchers - step 3

    # call matcher for each validators
    for validator in validators_list['validators']:
        # loop on matchers list , break the validator output into N parts
        matcher_index = 0
        for matcher in matchers_list['matchers']:
            get_matcher_rest_api = "http://127.0.0.1:%s/az/api/v1.0/matcher/validate/%s/%s/%s" % (
                matcher['port'], matcher['name'], request_id, validator['name'])
            h = httplib2.Http()
            resp, content = h.request(
                uri=get_matcher_rest_api,
                method='POST',
                headers={'Content-Type': 'application/json; charset=UTF-8'},
                body=json.dumps(matcher_fields_dict[matcher['name']]),
            )
    # get results from aggregator
    aggregator_get_rest_api = "http://127.0.0.1:%s/az/api/v1.0/aggregator/%s" % (
        app.config["AGGREGATOR_PORT"], request_id)

    agregator_content = urllib2.urlopen(aggregator_get_rest_api).read()

    return agregator_content


def call_validator(port_number, validator_fregment, request_id, validator_name, validator_schema):

    validator_call_suceesfull = True
    try:
        app.logger.info(
            'in call_validator send to validator ' + validator_name)

        validator_rest_api = "http://127.0.0.1:%s/az/api/v1.0/validator/%s/%s/%s/%s" % (
            port_number, validator_fregment, request_id, validator_name, validator_schema)
        h = httplib2.Http()

        resp, content = h.request(validator_rest_api, "GET")
    except Exception, e:
        app.logger.info('falied to  call_validator  ' +
                        validator_name + ' on port ' + port_number + ' ' + str(e))
        validator_call_suceesfull = False
    return validator_call_suceesfull


@app.route('/az/api/v1.0/validator/<string:validator_fregment>/<string:request_id>/<string:validator_name>/<string:validator_schema>', methods=['GET', 'POST'])
def validate_entity(validator_fregment, request_id, validator_name, validator_schema):

    app.logger.info('validating for memeber ' + validator_name)
    app.logger.info('query the validator db using the key fregment  ' + validator_fregment)

    validator_resultset = get_results_from_validator_db(
        validator_fregment, validator_schema)
    app.logger.info('validator has return ' + str(len(validator_resultset)) +  ' potential matches')

    app.logger.debug('validator display matching results  ' +
                     json.dumps(validator_resultset))
    # loop on matchers list , break the validator output into N parts
    matcher_index = 0
    for matcher in matchers_list['matchers']:
        # break the validator query output for multiple dict
        new_validator_resultset = dict()
        matcher_data_list = list()
        for validator_record in validator_resultset:

            email_matchers_frement = textwrap.wrap(validator_record[0], len(
                validator_record[0]) / matchers_count)[matcher_index]
            first_name_matchers_frement = textwrap.wrap(validator_record[1], len(
                validator_record[1]) / matchers_count)[matcher_index]
            last_name_matchers_frement = textwrap.wrap(validator_record[2], len(
                validator_record[2]) / matchers_count)[matcher_index]
            credit_card_matchers_frement = textwrap.wrap(validator_record[3], len(
                validator_record[3]) / matchers_count)[matcher_index]
            ip_address_matchers_frement = textwrap.wrap(validator_record[4], len(
                validator_record[4]) / matchers_count)[matcher_index]

            # create a new dict with fields and the marcher fregment and add it to the list
            record_dict = dict({'record_id': validator_record[5],'email': email_matchers_frement, 'first_name': first_name_matchers_frement,
                                'last_name': last_name_matchers_frement, 'credit_card': credit_card_matchers_frement, 'ip_address': ip_address_matchers_frement})
            matcher_data_list.append(record_dict)

        send_validation_results_to_matcher(
            matcher['port'], request_id, validator_name, matcher_data_list, matcher['name'])
        matcher_index = matcher_index + 1

    return jsonify({'validate_entity_json': 'done'})


def send_validation_results_to_matcher(matcher_port, request_id, validator_name, data, matcher_name):

    matcher_set_rest_api = "http://127.0.0.1:%s/az/api/v1.0/matcher/%s/%s/%s" % (
        matcher_port, request_id, validator_name, matcher_name)
    h = httplib2.Http()
    app.logger.info(
        'in validators send results to matcher via api  ' + matcher_set_rest_api)

    resp, content = h.request(
        uri=matcher_set_rest_api,
        method='POST',
        headers={'Content-Type': 'application/json; charset=UTF-8'},
        body=json.dumps(data),
    )


@app.route('/az/api/v1.0/matcher/<string:request_id>/<string:validator_name>/<string:matcher_name>', methods=['POST'])
def matcher_set_validation_results(request_id, validator_name, matcher_name):
    # get data from post request to save in cache
    validation_data = request.data
    matcher_cache_key = request_id + "_" + validator_name + "_" + matcher_name
    app.logger.debug(
        'in matcher_set_validation_results matcher key ' + matcher_cache_key)

    matcher_cache.set(matcher_cache_key, validation_data, 10)
    return jsonify({'set_validation_results': matcher_cache_key})


@app.route('/az/api/v1.0/matcher/validate/<string:matcher_name>/<string:request_id>/<string:validator_name>', methods=['GET', 'POST'])
def get_matcher_results(matcher_name, request_id, validator_name):


    if request.data is not None:
        app.logger.debug('get_matcher_results request.data ' +
                        str(request.data))

        matcher_fields_dict = json.loads(request.data)
        app.logger.debug('get_matcher_results ' + str(matcher_fields_dict))

    match_found = False
    matcher_cache_key = request_id + "_" + validator_name + "_" + matcher_name
    app.logger.debug(
        'in get_matcher_results matcher_cache_key ' + matcher_cache_key)

    validate_resultset_string = matcher_cache.get(matcher_cache_key)
    app.logger.debug(
        'in validate_resultset_string  ' + validate_resultset_string)
    output = dict()
    if validate_resultset_string is not None:
        validate_resultset = json.loads(validate_resultset_string)
        app.logger.info(' Porcessing results from validator ' + validator_name)
        app.logger.info('   Matching for email fregment ' + matcher_fields_dict['email'])

        for validator_record in validate_resultset:
            app.logger.info('      Comparing to ' +validator_record['email'] + ' matching ' + str (validator_record['email'] == matcher_fields_dict['email']))
            all_fields_results = dict()
            # set the results of partial matches
            all_fields_results.update(
                {'email': validator_record['email'] == matcher_fields_dict['email']})
            all_fields_results.update(
                {'first_name': validator_record['first_name'] == matcher_fields_dict['first_name']})
            all_fields_results.update(
                {'last_name': validator_record['last_name'] == matcher_fields_dict['last_name']})
            all_fields_results.update(
                {'credit_card': validator_record['credit_card'] == matcher_fields_dict['credit_card']})
            all_fields_results.update(
                {'ip_address': validator_record['ip_address'] == matcher_fields_dict['ip_address']})
            #break
            output.update(
                {validator_record['record_id']:all_fields_results})

        #if match_found == False:
        #    app.logger.info('           No match  ' )

    #output = dict({'email_match': match_found,
    #               'fields_results': all_fields_results})
    send_matcher_results_to_aggregator(
        request_id, validator_name, output, matcher_name)

    return jsonify(output)


def send_matcher_results_to_aggregator(request_id, validator_name, data, matcher_name):

    aggregator_set_rest_api = "http://127.0.0.1:%s/az/api/v1.0/aggregator/%s/%s/%s" % (
        app.config["AGGREGATOR_PORT"], request_id, validator_name, matcher_name)
    h = httplib2.Http()
    app.logger.info(
        'in matcher send results to aggregator via api  ' + aggregator_set_rest_api)

    resp, content = h.request(
        uri=aggregator_set_rest_api,
        method='POST',
        headers={'Content-Type': 'application/json; charset=UTF-8'},
        body=json.dumps(data),
    )


@app.route('/az/api/v1.0/aggregator/<string:request_id>/<string:validator_name>/<string:matcher_name>', methods=['POST'])
def aggregator_set_matcher_results(request_id, validator_name, matcher_name):
    # get data from post request to save in cache
    aggregator_data = request.data
    aggregator_cache_key = "aggregator_" + request_id + \
        "_" + validator_name + "_" + matcher_name
    app.logger.debug(
        'in aggregator_set_matcher_results aggregator key ' + aggregator_cache_key)

    aggregator_cache.set(aggregator_cache_key, aggregator_data, 10)
    return jsonify({'set_aggregator_data': aggregator_data})


@app.route('/az/api/v1.0/aggregator/<string:request_id>', methods=['GET'])
def get_aggregator_results(request_id):

    aggregator_results_dict = {
        'request_id': request_id,
        'matching_results': dict(),
    }

    # loop on each validator , and then for each matcher
    for validator in validators_list['validators']:

        validator_match = True
        app.logger.info(
            'Aggregator results for validator ' +  validator['name'])

        validator_matching_records = dict()

        validator_matching_printout_dict = dict()
        printout_index =1
        for matcher in matchers_list['matchers']:

            aggregator_cache_key = "aggregator_" + request_id + \
                "_" + validator['name'] + "_" + matcher['name']

            matcher_resultset_string = aggregator_cache.get(
                aggregator_cache_key)
            app.logger.debug(
                'in get_aggregator_results  ' + matcher_resultset_string)

            if matcher_resultset_string is not None:
                matcher_results_dict = json.loads(matcher_resultset_string)

                # loop on all records from matcher
                for record_id , matching_results_dict in matcher_results_dict.iteritems():
                    if record_id in validator_matching_records:

                        validator_matching_records[record_id]['email'] = validator_matching_records[
        record_id]['email'] and matching_results_dict['email']

                        validator_matching_records[record_id]['first_name'] = validator_matching_records[
                record_id]['first_name'] and matching_results_dict['first_name']

                        validator_matching_records[record_id]['last_name'] = validator_matching_records[
                record_id]['last_name'] and matching_results_dict['last_name']

                        validator_matching_records[record_id]['credit_card'] = validator_matching_records[
                record_id]['credit_card'] and matching_results_dict['credit_card']

                        validator_matching_records[record_id]['ip_address'] = validator_matching_records[
                record_id]['ip_address'] and matching_results_dict['ip_address']


                    else:
                        validator_matching_records.update(
                            {record_id:matching_results_dict})
                        validator_matching_printout_dict [record_id] = dict()

                        # create a dict for printing the output

                    validator_matching_printout_dict[record_id].update(
                        {'email'+str(printout_index):str(int(matching_results_dict['email']))})
                    validator_matching_printout_dict[record_id].update(
                        {'first_name'+str(printout_index):str(int(matching_results_dict['first_name']))})
                    validator_matching_printout_dict[record_id].update(
                        {'last_name'+str(printout_index):str(int(matching_results_dict['last_name']))})
                    validator_matching_printout_dict[record_id].update(
                        {'credit_card'+str(printout_index):str(int(matching_results_dict['credit_card']))})
                    validator_matching_printout_dict[record_id].update(
                        {'ip_address'+str(printout_index):str(int(matching_results_dict['ip_address']))})

                printout_index =printout_index +1

        # loop on records for print output
        print_aggregator_results(validator_matching_printout_dict,printout_index)

        validator_match = False
        matching_record_id = -1
        for record_id , matching_results_dict in validator_matching_records.iteritems():
                if matching_results_dict['email']==True :
                        validator_match=True
                        matching_record_id=record_id
                        #assuming for now there is at most single match
                        break
        # check match in the validator level
        if validator_match == True:
            app.logger.info('there is a match for the validator  ' +
                            validator['name'])
            matching_output = dict(
                {'email_match': True, 'fields_results': validator_matching_records[record_id]})

        else:
            app.logger.info('there is no match for the validator  ' +
                            validator['name'])
            matching_output = dict({'email_match': False})
        aggregator_results_dict['matching_results'].update(
            {validator['name']: matching_output})

    return jsonify(aggregator_results_dict)


def print_aggregator_results(validator_matching_printout_dict, printout_index):
    table = BeautifulTable()
    table.row_separator_char = ''
    table.column_headers = ["rec", "email_1", "email_2", "first_name_1", "first_name_2",
                            "last_name1", "last_name2", "credit_card1", "credit_card2", "ip_address1", "ip_addres2"]
    table.column_alignments['rec'] = BeautifulTable.ALIGN_LEFT
    for record_id, matching_results_dict in validator_matching_printout_dict.iteritems():
        app.logger.debug("dict " + str(matching_results_dict))

        table.append_row([record_id, matching_results_dict["email1"], matching_results_dict["email2"],
                          matching_results_dict["first_name1"], matching_results_dict["first_name2"],
                          matching_results_dict["last_name1"], matching_results_dict["last_name2"],
                          matching_results_dict["credit_card1"], matching_results_dict["credit_card2"],
                          matching_results_dict["ip_address1"], matching_results_dict["ip_address2"]])

    print(table)


def get_results_from_validator_db(email_validator_fregment, validator_schema):

    validation_sql = "SELECT substr(md5(email),3,30) email, \
    md5(first_name) first_name, md5(last_name) last_name,\
    md5(credit_card) credit_card, md5(ip_address) ip_address , id \
    FROM %s.users \
    where substr(md5(email),1,2) = \"%s\" " % (validator_schema, email_validator_fregment)

    app.logger.debug('in validate_entity validation_sql ' + validation_sql)

    cursor.execute(validation_sql)
    data = cursor.fetchall()

    return data


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.validate_on_submit():
        redirect_url = '/az/api/v1.0/requestor/%s' % (form.email)
        app.logger.info('signup redirect to ' + redirect_url)
        input_fields_dict = dict()
        input_fields_dict.update(
            {'first_name': form.first_name.data})
        input_fields_dict.update(
            {'last_name': form.last_name.data})
        input_fields_dict.update(
            {'email': form.email.data})
        input_fields_dict.update(
            {'ip_address': '88.191.38.85'})
        input_fields_dict.update(
            {'credit_card': form.creditcard.data})
        input_fields_dict.update(
            {'zipcode': form.zipcode.data})
        # run post request to indentiq

        requester_api = "http://127.0.0.1:%s/az/api/v1.0/requestor/" % (
            app.config["SERVER_PORT"])
        h = httplib2.Http()

        app.logger.info(
            ' signup form call requestor  ' + str(json.dumps(input_fields_dict)))

        resp, content = h.request(
            uri=requester_api,
            method='POST',
            headers={'Content-Type': 'application/json; charset=UTF-8'},
            body=json.dumps(input_fields_dict),
        )
        matching_output = json.loads(str(content))

        return jsonify(matching_output)
    return render_template('index.html', title='Sign up', form=form)


if __name__ == '__main__':
    app.run(debug=True, port=app.config["SERVER_PORT"])
