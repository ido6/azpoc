DEBUG = True # Turns on debugging features in Flask
# MySQL configurations


MYSQL_DATABASE_USER = 'root'
MYSQL_DATABASE_PASSWORD = 'az123456'
MYSQL_DATABASE_DB = 'az_uber'
MYSQL_DATABASE_HOST = 'localhost'

secret_key = 'super uber key'
SECRET_KEY = 'super uber key'

SESSION_TYPE = 'filesystem'

LOG_FILE_NAME = 'logs/single.log'
SERVER_PORT = 5000
MATCHER_PORT = 5000
AGGREGATOR_PORT = 5000
