# requester configurations
# MySQL configurations
MYSQL_DATABASE_USER = 'root'
MYSQL_DATABASE_PASSWORD = 'az123456'
MYSQL_DATABASE_DB = 'az_uber'
MYSQL_DATABASE_HOST = 'localhost'

secret_key = 'super uber key'
SECRET_KEY = 'super uber key'

LOG_FILE_NAME = 'logs/validator.log'
SERVER_PORT = 5000
AGGREGATOR_PORT = 5300
