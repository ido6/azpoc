# matcher config
# MySQL configurations
MYSQL_DATABASE_USER = 'root'
MYSQL_DATABASE_PASSWORD = 'az123456'
MYSQL_DATABASE_DB = 'az_uber'
MYSQL_DATABASE_HOST = 'localhost'

secret_key = 'super uber key'

LOG_FILE_NAME = 'logs/matcher.log'
SERVER_PORT = 5001
MATCHER_PORT = 5001
AGGREGATOR_PORT = 5000
