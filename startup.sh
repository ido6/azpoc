#!/bin/sh
osascript <<END
tell application "Terminal"
    do script "cd projects/azpoc/azpoc/ ; export AZPOC_CONFIG_FILE=config/requesterconfig.py ; python az-server.py"
    do script do script "cd projects/azpoc/azpoc/ ; export AZPOC_CONFIG_FILE=config/uberconfig.py ; python az-server.py"
    set current settings of window 1 to settings set "Novel"

end tell
END
