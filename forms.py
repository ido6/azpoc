from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField , HiddenField
from wtforms.validators import DataRequired

class SignupForm(FlaskForm):
    first_name = StringField('Firstname', validators=[DataRequired()], render_kw={"placeholder": "First Name"})
    last_name = StringField('Lastname', validators=[DataRequired()], render_kw={"placeholder": "Last Name"})
    email = StringField('Email', validators=[DataRequired()], render_kw={"placeholder": "Email"})
    creditcard = StringField('Creditcard', validators=[DataRequired()], render_kw={"placeholder": "Credit Card"})
    zipcode = StringField('Zipcode', validators=[DataRequired()], render_kw={"placeholder": "Zip Code"})
    ip_address = HiddenField('IP_Address', default='88.191.38.85')
    submit = SubmitField('Signup')
